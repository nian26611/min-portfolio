# Intro til it sikkerhed opgaver

## Dokumentation opbygning

1. Beskrivelse af opgaven
2. Hvordan kan andre reproducere det du har gjort for at udføre opgaven, trin for trin guides er ofte gode til det.
3. Hvilke problemer havde du i udførelsen og hvad gjorde du for at løse dem.
4. Hvilke ressourcer (links og andre kilder) har du anvendt
5. Opsummering af tilegnede erfaringer

## Opgave 1 Markdown

### Opgave Beskrivelse

I denne opgave har jeg skulle øve mig i Markdown syntax og prøve at bruge de forskellige componenter der er til rådighed.

**Her er et link til en hjemmeside**

[Link til google](https://www.google.com)

**Her er et billede som kommer fra et eksternt link**

![Tux, the Linux mascot](https://global.discourse-cdn.com/business6/uploads/coda1/original/3X/7/2/72507cd4f9b289d55c379be4c64fca6484ec768b.jpeg)

| Syntax      | Description | Description | Description |
| :---        |:-----------:|:-----------:|        ---: |
| Header      | Title       | Title       | Title       |
| Paragraph   | Text        | Text        | Text        |
| Paragraph   | Text        | Text        | Text        |
| Paragraph   | Text        | Text        | Text        |

**Unordered List**

- Første 1
- Anden 1
    - Anden 2
        - Anden 3
- Tredje 1
- Fjerde 1

**Ordered list**

1. Første 1
2. Anden 1
3. Tredje 1
    1. Tredje 2.1
    2. Tredje 2.2
4. Fjerde

**Her er fed tekst**

**Fed tekst**

kun lidt **fed** tekst

**her er italic text**

*italic text er bare pænt*

lidt ***italic*** text er også *pænt*

**Her er der nogle eksempler på code boxes**

Dette er HTML

    <html>
      <head>
      </head>
    </html>

Dette er C#

    namespace HelloWorld
    {
      class Program
      {
        static void Main(string[] args)
        {
          Console.WriteLine("Hello World!");    
        }
      }
    }

Dette er SQL

    UPDATE Customers
    SET ContactName='Alfred Schmidt', City='Frankfurt'
    WHERE CustomerID=1;

**Hvordan løste jeg denne ogpave?**

Til at løse denne opgave har jeg benyttet følgende hjemmesider med eksempler på syntax og lignende.

[https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)

[https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/)

[https://www.w3schools.io/file/markdown-introduction/](https://www.w3schools.io/file/markdown-introduction/)


## Opgave 25 - Kali Linux 

**VPN (Virtual Private Network)**

En VPN kan man bruge til at forbinde sig krypteret til et andet Netværk

**SSH (Secured Shell)**

SSH er en krypteret fjernadgang til en anden maskine. Man kan bruge SSH til at få adgang til en maskine på ens netværk. Man kan også lave en forbindelse til en maskine på et andet netværk hvis netværket har en port åben til det.

**Command lines**

- cd - Change directionary (eks. cd ./testfolder)
- ls - listing (printer en liste ud af filer i det directionary man befinder sig i)
- find - finder lokationen for en eller flere filer (eks. find -name passwords.txt & find -name *.txt (finder filer der indeholder .txt))
- cat - concatenate (viser indhold af fil eks. cat note.txt)
- grep - kan søge efter en string i en fil ((eks. grep "Hello" note.txt) (output Hello World!))
- echo - udskriver det du skriver i terminalen (eks. ecco "Hello World")

**Operators**

- "&"	This operator allows you to run commands in the background of your terminal.
- "&&"	This operator allows you to combine multiple commands together in one line of your terminal.
- ">"	This operator is a redirector - meaning that we can take the output from a command (such as using cat to output a file) and direct it elsewhere.
- ">>"	This operator does the same function of the > operator but appends the output rather than replacing (meaning nothing is overwritten).

|Command	|Full Name	|Purpose|
|touch	|touch	|Create file|
|mkdir	|make directory|	Create a folder|
|cp	|copy	|Copy a file or folder|
|mv	|move	|Move a file or folder|
|rm	|remove	|Remove a file or folder|
|file	|file	|Determine the type of a file|



## Opgave 28 - CIA Modellen

Dine data på computere og cloud (...)

**Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.**

- C - Man vil ikke have at andre har adgang til ens personlige data og filer.

- I - Man vil ikke have at andre kan have ændret i ens filer.

- A - Man vil altid have mulighed for at tilgå ens data.


**Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)**

- Randsomware angreb (hvis ens computer biver krypteret og man ikke kan tilgå ens filer eller programmer)
- DDos angreb (Hvis den der hoster ens Cloud bliver DDos'ed kan man ikke tilgå ens data)
- Phishing (kan installere malware på din computer eller stjæle data som passwords eller lignende)
- Tojansk hest (en anden person får fjernadgang til din computer, kan stjæle din data benytte din computer og lignende)

**Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)**

- Backup af data - A & I
- Multi factor authentication - C
- Kryptering - I & C
- Stærke passwords - C
- Kryptering - C (Kryptering af ens harddisk)


## Opgave 26 - Wireshark analyse

Opgaven går ud på at jeg skal analysere en .pcap fil fra mawlware-traffic-analysis.net.

Jeg har hentet filen fra følgende link: [pcapFil](https://www.malware-traffic-analysis.net/2014/12/15/2014-12-15-traffic-analysis-exercise.pcap.zip)

passworded til at kunne udpakke filen var "infected", jeg fandt passworded på "about" siden hvor jeg hentede filen fra.

https://ucl-pba-its.gitlab.io/exercises/intro/26_intro_wireshark_analyse/

## Opgave 16 - Simpelt netværk: Virtuelle maskiner i vmware

Opgaven går ud på at opsætte 2 xubuntu vm maskiner op. Jeg benytter følgende xubuntu images [https://xubuntu.org/download/](https://xubuntu.org/download/).

Jeg opsætter følgende netværk i mit Vmware

![RouterSpecs](./images/16NetvaerkBillede.png)

https://ucl-pba-its.gitlab.io/exercises/intro/16_intro_opgave_vmware_xubuntu/

## Opgave 17 - Opsætning af vsrx virtuel router

I denne øvelse opsætter jeg en virtuel juniper vsrx (juniper srx240) router.

https://ucl-pba-its.gitlab.io/exercises/intro/17_intro_opgave_vmware_vsrx_1/#information

## Opgave 18 - Seriel forbindelse til vsrx virtuel router

I denne øvelse skal jeg sætte en vsrx router op, tilføje en serial forbindelse. Konfigurere denne fobindelse i putty  med følgende parametre:

- baudrate 9600
- 8 databits
- 1 stopbit
- parity + flowcontrol none

da jeg så bootede min vsrx og forbandt med putty, kunne jeg i putty se det samme skete i vsrx consolen. Jeg har nu fjernadgang til vsrx fra min windows maskine. (default user er root med intet password til vsrx)

https://ucl-pba-its.gitlab.io/exercises/intro/18_intro_opgave_vmware_vsrx_2/#information

## Opgave 19 konfigurer vsrx password og navn

I denne opgave har jeg konfigureret navn samt password til min vsrx maskine.

https://ucl-pba-its.gitlab.io/exercises/intro/19_intro_opgave_vmware_vsrx_3/#information

## Opgave 20 - konfigurer vsrx routing

I denne opgave opsætter jeg interfaces til mine subnets

Jeg kan ikke pinge imellem mine 2 enheder, jeg har fulgt opsætningen som vist i videoen her under. Det ligner at mine enheder ikke kan forbinde til netværket?

https://ucl-pba-its.gitlab.io/exercises/intro/20_intro_opgave_vmware_vsrx_4/

## Opgave 21 - tilføj interface til vsrx

Jeg fulgte guiden i opgaven som der er linket til under. jeg løb dog stadig ind i problemer med at få forbindelse imellem enhederne på netværket..

https://ucl-pba-its.gitlab.io/exercises/intro/21_intro_opgave_vmware_vsrx_5/#links

## Opgave 22 - Kali linux vm på netværket

Siden jeg ikke har kunne få vsrx enheden til at virke, ,har jeg valgt at bruge et image til en anden router. Jeg har her fået forbindelsen imellem min kali maskine og min xubuntu maskine til at virke.

## Opgave 23 - Damn Vulnerable Web Application (DVWA)

Jeg fulgte nedenstående link til at installere DVWA.

https://www.youtube.com/watch?v=WkyDxNJkgQ4

for at starte DVWA skal man køre "sudo service apache2 start" i consolen.
