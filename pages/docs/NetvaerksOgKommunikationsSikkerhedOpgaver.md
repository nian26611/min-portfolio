# Netværk- og kommunikationssikkerhed Opgaver


## **Opgave 1 Fagets læringsmål**

Find læringsmålene i studieordningen på [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed) og del linket mellem jer

I jeres team diskuter hvordan i forestiller jer at læringsmålene er relevante for faget og hvad i kan bruge dem til, begrund jeres valg og noter dem så vi kan diskutere det på klassen.
 
 
### **Opgave løsning**

Hovedemnerne virker relevante for emnet

Et link til intruduktion til netværk, protokoler og lignende. [https://www.youtube.com/watch?v=vrh0epPAC5w](https://www.youtube.com/watch?v=vrh0epPAC5w)


## **Opgave 2**

**find ud af forskellig information og specs omkring router og lignende, billede og kopi af disse specs**

![Router](https://www.proshop.dk/Images/915x900/3126566_ac6e7a6ff341.jpg)

**Producent**: ZyXEL

**Varenummer**: 3126566

**Model**: EX3300-T0-EU01V1F

**Ean**: 4718937618422

![RouterSpecs](./images/RouterSpecs.PNG)

### **opgave 2.1**

**Applicaion**

Dette lag af OSI modellen er det lag der er tættest på brugeren. Det er her at brugeren kan se og manipulere data.

**Presentation**

Pesentation laget formatere dataen enten fra netværket til applikationen, eller omvendt. Det er for eksempel her at data bliver decrypoteret.

**Session**

Session laget opretter og opretholder sessions mellem systemer, eksempelvis hvor lang tid et system skal vente på svar samt hvornår en session skal afbrydes

**Transport**

Transport laget benytter: 

- Transmission Control Protocol (TCP) - Håndtryksaftale mellem afsender og modtager
- User Datagram Protocol (UDP) - Afsender sender packages afsted og er ligeglad med om de bliver modtaget

Disse protocoler har hver især 65.536 port numre, som bliver benyttet af services, programmer og lignende til at sende og modtage packages. Transport laget består ikke af noget konkret hardware, men nærmere software som sender data frem og tilbage.

**Network**

Network laget er ansvarlig for at sende packages fra en ende til en anden. Den benytter Internet Protocol adresser (IP Adress) til at identificere alle der er koplet op på internettet.

- Router - En routers ansvar er at facilitere kommunikationen imellem forskellige netværk. Den opdeler det lokale netværk og det store internet, da man kun kan kommunikere med interne enheder hvis man ikke har en router til at facilitere kommunikationen med internetet

- Logiske adresser(IP)

**Data link**

Data link laget, håndtere at overføre packages fra en NIC til en anden

Bruger MAC adresser - fysiske adresser.

- Network Interface Card (NIC) - Det er en NIC man indsæætter sine kapler i, som håndtere binær kode til og fra kapler
- WiFi Network Interface Card (WiFi NIC) - Virker på samme møde, den modtager og sender radiobølger som bliver fortolket som binær kode

**Physical**

Det fysiske lag har ansvare for at overføre bits

- Wifi 
- Ethernet kapler
- Fiber kapler
- Repeaters (gentager kaldet)
- Hub (multi port repeater, hvis 4 enheder er koblet på hubben, og en laver et kald, bliver det repeatet til de 3 andre enheder)

fysiske enheder har en mac adresse som er den physiske adresse(den fysiske pan dang til IP adresse).

**Link til løsning af opgave**

[https://www.practicalnetworking.net/series/packet-traveling/osi-model/](https://www.practicalnetworking.net/series/packet-traveling/osi-model/)

### **opgave 2.2**

Find eksotiske eksempler på enheder:

- Rasperry Pi - En mini computer der er nem at gemme, kan bruges til at scanne netværk og fange data (kilde: [https://www.raspberrypi.org/help/what-%20is-a-raspberry-pi/](https://www.raspberrypi.org/help/what-%20is-a-raspberry-pi/) )


## **Opgave 3**

**Fejl i build af pipeline**

Jeg pushede min ændringer til GIT, min pipeline fejlede i build og jeg fik følgende fejlmeddelelse.

![Error](./images/yamlerror.PNG)

Det ligner at fejlen opstod i yaml filen jeg pushede op. For at kontrollere hvad fejlen i filen kunne være, fandt jeg eksempler på yaml syntax på internetet. Denne syntax sammenlignede jeg med min yaml fil, og fandt ud af at jeg skulle indente nogle af linjerne i filen. Jeg pushede filen op igen og buildede pipelinen, som derefter buildede korrekt

![ErrorFixed](./images/yamlerrorfixed.PNG)

## **Opgave 4 - specs**

- Router Zyxel EX3300-T0-EU01V1F

![RouterSpecs](./images/RouterSpecs.PNG)

Præstation

- Maksimum datahastighed (2,4 GHz): 600 Mbps
- Maksimum datahastighed (5 GHz): 1,2 Gbps

Dette er routerens maksimale datahastighed over WiFi

Dette virker som høje tal, men afstanden imellem router og enhed spiller ind på den reelle maks hastighed.
Hvis man benytter 2,4 GHz vil der være en mere stabil hastighed på afstand. Hvorimod med 5 GHZ vil datahastigheden være stærkere på kortere afstand, men rækkevidden vil være mindre

Routeren kan håndtere 4 SSID(Service Set IDentifier - netværks navne), med disse kan man blandt andet lave et gæste netværk som ikke har adgang til de samme enheder eller brugere som "hovednetværket".    

## **Opgave 5 - internet speeds**

**Hvad er hastigheden?**

Download - 91.6 mbps

Ping 5 ms (ping er rejsen fra punkt A til punkt B)

Upload 97.8

Jitter 1 ms (jitter er støj på linjen, hvis ens kald bliver omdirigeret, og forsinket pga "kø" på de forskellige servere som kaldet går igennem)

**Best guess: hvad er bottleneck, og hvorfor?**

Hvis flere personer benytter internettet samtidigt, vil man ikke kunne udnytte alle de mbps der er til rådighed samtidigt.

## **Opgave 6 - more internet speed**

![RouterSpecs](./images/Iperf3Example.PNG)

Hastigheden på første kald var lidt svingende, hvilket giver god mening når det er mit netværk det kalder serveren. Jeg lavede kaldet imens min kæreste brugte internetet, hvilket måske kunne være en grund til de svingende svar tider.

Andet kald var mere stabilt, hvilket giver god mening når det er en stor ekstern server der kalder. Dens netværk vil være mere stabilt, da det har flere krafter og resurser end mit netværk. - det burde måske være ens??

Jeg stødt ind i at jeg ikke kunne forbinde til de server jeg prøvede på Iperfs hjemmeside. Efter at have prøvet at kalde nogle andre porte og lignende, opdagede jeg at sidste version af Iperf kom ud i 2016. Jeg tænkte at listen med servere man kunne kalde måske var outdated, og jeg prøvede derfor at starte fra en ende af og kalde alle serverne en af gangen. Jeg fik efter lidt tid hul igennem til den Ukrainske server og jeg kunne derefter færdigøre opgaven.

## Opgave x1 - logisk & fysisk diagram over netværk (draw IO til at tegne netværksdiagram)

**Fysisk Diagram**

![Fysisk Diagram](./images/FysiskDiagram.PNG)

**Logisk Diagram**

![Logisk Diagram](./images/LogiskDiagram.PNG)

Fin skriv de 2 diagrammer fra undervisning - 20/02, beskriv de forskellige forbindelser og enheder og beskriv hvad der sker mellem enheder og hvordan der bliver kommunikeret.

## Opgave 7 - Netbox ipam ##

Hvad er Netbox?

- Single source of truth
- API (web)

Hvad er netbox ikke?

- Scanner (netværk)

I denne opgave sætter jeg Netbox op i mit VmWare. Når den starter den virtuelle maskine, kan jeg åbne browseren og tilgå Netbox brugergrænseflade. Her Prøver jeg at sætte forskellige ting op i deres miljø, og få en forståelse for hvordan Netbox virker.

## Netflow Opgave ##

**Hvad er Netflow**

Netflow er et værktøj man kan bruge til at få informationer omkring forbindelser som går ud og ind af netværket.


