# System Sikkerhed Opgaver

## Læringsmål for system sikkerhed med eksempler

**Viden**

Den studerende har viden om:

- Generelle governance principper / sikkerhedsprocedurer (Regler og retningslinjer for it sikkerhed på ledelsesniveau, CIS 18, ISO, GDPR)
- Væsentlige forensic processer (Wireshark)
- Relevante it-trusler (phising og ransomware)
- Relevante sikkerhedsprincipper til systemsikkerhed (CIA, firewall, kryptering)
- OS roller ift. sikkerhedsovervejelser (opsætning af roller på ad niveau)
- Sikkerhedsadministration i DBMS. (opsætning af sikkerhed, eksemplvis roller i forskellige slags databaser, relationelle, hirakiske osv)

**Færdigheder**

Den studerende kan:

- Udnytte modforanstaltninger til sikring af systemer (intrusion prevention system (IPS))
- Følge et benchmark til at sikre opsætning af enhederne (stress test af system (DDOS))
- Implementere systematisk logning og monitering af enheder (triggers på forsøg på adgang diverse steder, SQL, førsøg på login, ændring af data og lignende)
- Analysere logs for incidents og følge et revisionsspor (SO logs eks. linus logs og windows eventmanager)
- Kan genoprette systemer efter en hændelse (backup, oprydning af mailicious filer, udlukning af hackere der har fået adgang til ens system).

**Kompetencer**

Den studerende kan:

- håndtere enheder på command line-niveau (bash, powershell)
- håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler (concurrency?)
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at
forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser (It governance, ISO, CIS 18).
- håndtere relevante krypteringstiltag (https, hashing(ikke krypteret))

**ECTS-omfang**

Fagelementet System Sikkerhed har et omfang på 10 ECTS-point.


## **Opgave 3 - Navigation i Linux filesystem med bash**

**Instruktioner**
- eksekver kommandoen pwd
- eksekver kommandoen cd ..
- eksekver kommandoen cd /
- Lokationen / har en bestemt betydning i Linux file systemet. Hvad er det? (Her kan du med fordel søge svar med Google)

Det er roden af alle directories, det er ikke et directory i sig selv, men et directory seperator.

- eksekver kommandoen cd /etc/ca-certificates/
- Hvor mange Directories viser outputtet fra pwd kommandoen nu?

1

- eksekver kommandoen cd ../..
- Hvor mange Directories viser outputtet fra pwd kommandoen nu?

0

- eksekver kommandoen cd ~ (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6)
- Kommandoen ~ er en "Genvej" i linux, hvad er det en genvej til?

ens home directory

- i file systemets rod(/), esekver kommandoen ls
- i brugerens Home directory, eksekver kommandoen touch helloworld.txt
- i brugerens Home directory, eksekver kommandoen touch .helloworld.txt
- list alle filer og mapper i brugerens Home directory
- list alle filer og mapper i brugerens Home directory med flaget -a

--All viser alle entries

- list alle filer og mapper i brugerens Home directory med flaget -l

bruger en long list format

- list alle filer og mapper i brugerens Home directory med flaget -la

viser alle entries med long list format

- i brugerens Home directory, eksekver kommandoen mkdir helloWorld

Make Directory - laver et nyt directory

- Eksekver kommandoen ls -d */

-d leder efter directories men ikke deres inhold, -d */ vil finde directories der er i nuværende directory

- Eksekver kommandoen ls -f

viser ikke farve på directories og listen er ikke sorted


## **Opgave 4 - Linux file struktur**

| Directory | Description |
|----|----|
| /	|The nameless base of the filesystem. All other directories, files, drives, and devices are attached to this root. Commonly (but incorrectly) referred to as the “slash” or “/” directory. The “/” is just a directory separator, not a directory itself. |
| /bin | Essential command binaries (programs) are stored here (bash, ls, mount, tar, etc.) |
| /boot	| Static files of the boot loader. |
| /dev | Device files. In Linux, hardware devices are accessed just like other files, and they are kept under this directory. |
| /etc | Host-specific system configuration files. |
| /home	| Location of users' personal home directories (e.g. /home/Susan). |
| /lib | Essential shared libraries and kernel modules. |
| /proc	| Process information pseudo-filesystem. An interface to kernel data structures. |
| /root	| The root (superuser) home directory. |
| /sbin	| Essential system binaries (fdisk, fsck, init, etc). |
| /tmp	| Temporary files. All users have permission to place temporary files here. |
| /usr	| The base directory for most shareable, read-only data (programs, libraries, documentation, and much more). | 
| /usr/bin	| Most user programs are kept here (cc, find, du, etc.). |
| /usr/include	| Header files for compiling C programs. |
| /usr/lib	| Libraries for most binary programs. |
| /usr/local | “Locally” installed files. This directory only really matters in environments where files are stored on the network. Locally-installed files go in /usr/local/bin, /usr/local/lib, etc.). Also often used for software packages installed from source, or software not officially shipped with the distribution. |
| /usr/sbin	| Non-vital system binaries (lpd, useradd, etc.) |
| /usr/share | architecture-independent data (icons, backgrounds, documentation, terminfo, man pages, etc.). |
| /usr/src | Program source code. E.g. The Linux Kernel, source RPMs, etc. |
| /usr/X11R6 |The X Window System. |
| /var | Variable data: mail and printer spools, log files, lock files, etc. |
| /opt | Optional or third party software. |



## **Opgave 5 - Hjælp i Linux**

- Eksekver kommandoen apt --help.
- via apt --help skal du nu finde ud af hvordan du udskriver en liste til konsolen, som viser alle installeret pakker.

apt show - viser de pakker der er installeret.

- Eksekver kommandoen apt -h

viser også help

- Eksekver kommandoen man apt og scroll ned i bunden af konsol outputtet(pgdwn)

viser manualen for komandoen

- Eksekver kommandoen man ls og scroll ned i bunden af konsol outputtet

- Eksekver kommandoen man man og skim outputtet, hvilken information kan du finde der?

en manual til manualen, den viser generel information omkring kommandoer i BASH

- Eksekver kommandoen help


## Opgave 6 - Søgning i Linux file strukturen

- I Home directory, eksekver kommandoen find /etc/

finder alt der starter med /etc/

- I Home directory, eksekver kommandoen find

udskriver alt hvad der er i  directory samt hvad der er i under directories

- eksekver kommandoen sudo find /etc/ -name passwd sudo foran kommandoen betyder at kommandoen eksekveres med de rettigheder som sudo gruppen har

sudo giver en root rettigheder

- eksekver kommandoen sudo find /etc/ -name pasSwd Husk stort S

der skete ikke noget

- eksekver kommandoen sudo find /etc/ -iname pasSwd Husk stort S

- eksekver kommandoen sudo find /etc/ -name pass*

finder hvor navnet starter med "pass" * betyder at strengen kan indeholde mere.

- De næste kommandoer bruges til at finde filer baseret på deres byte størrelse. De to første trin bruges blot til at generer de to filer som skal findes
- I Home directory, eksekver kommandoen truncate -s 6M filelargerthanfivemegabyte
- I Home directory, eksekver kommandoen truncate -s 4M filelessthanfivemegabyte
- I roden(/), eksekver kommandoen find /home -size +5M

finder filer på over 5 megabyte

- I roden(/), eksekver kommandoen find /home -size -5M

finder filer som er mindre end 5 megabyte

- I Home directory, opret to directories. et der hedder test og et andet som hedder test2
- I test2, skal der oprettes en file som hedder test
- I Home directory, eksekver kommandoen find -type f -name test

finder filer med navne test

- I Home directory, eksekver kommandoen find -type d -name test

finder directories med navnet test

## **Opgave 7 Ændring og søgning i filer **

Instruktioner
- I Home directory, eksekver kommandoen touch minfile.txt

opretter filen minfile.txt

- I Home directory, eksekver kommandoen cp minfile.txt kopiafminfile.txt

laver en kopi af filen og navngiver den kopiafminfile.tx

- I Home directory, eksekver kommandoen mkdir minfiledir

laver et directory der hedder minfildir

- I Home directory, eksekver kommandoen, mv minfile.txt minfiledir

rykker minfile.txt til minfildir

- I Home directory, eksekver kommandoen, rm -r minfiledir

sletter directory minfiledir og txt filen der var derinde

I de næste trin skal der arbejdes med oprettelse af filer med tekst indhold, her bliver redirect operatoren introduceret (>). Operatoren tager outputet fra kommandoen på venstre side og skriver til filen på højre side.

- I Home directory, eksekver kommandoen echo "hej verden" > hejverdenfil.txt

filen hejverden.txt bliver oprettet og indeholder texten "hej verden"

- I Home directory, eksekver kommandoen cat hejverdenfil.txt

udskriver indholdet af txt filen hejverdenfil.txt

- I Home directory, eksekver kommandoen echo "hej ny verden" > hejverdenfil.txt

overskriver indholdet af hejverdenfil.txt til "hej ny verden"

- I Home directory, eksekver kommandoen cat hejverdenfil.txt

viser det nye indhold af filen

- I Home directory, eksekver kommandoen echo "hej endnu en ny verden" >> hejverdenfil.txt

tilføjer "hej endnu en ny verden" til indholdet af hejverdenfil.txt

- I Home directory, eksekver kommandoen cat hejverdenfil.txt

viser inholdet af filen som nu er:

"hej ny verden
hej endnu en ny verden"

I de næste trin introduceres pipe operatoren (|). Den tager outputtet fra kommandoen på venstre side, og giver det videre til kommandoen på højre side

- I /etc/, eksekver kommandoen cat adduser.conf.

udskriver indholdet af adduser.conf

- I /etc/, eksekver kommandoen cat adduser.conf | grep 1000.

tager texten i adduser.conf og leder efter "1000" i texten, og giver mig de linjer hvor 1000 indgår

- I /etc/, eksekver kommandoen cat adduser.conf | grep no.

tager texten i adduser.conf og leder efter "1000" i texten, og giver mig de linjer hvor no indgår

- I /, eksekver kommandoen grep no /etc/adduser.conf

denne kommando gør det samme som fornævnte kommando

- I /, eksekver kommandoen ls -al | grep proc

viser en long list af hvad der er i directionary "/" og leder efter "proc"

- I /etc/, eksekver kommandoen ls -al | grep shadow

viser en long list af hvad der er i directionary "/etc/" og leder efter "shadow"


## **Opgave 8 - Processer og services i Linux**


