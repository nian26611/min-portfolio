# Noter

## Netværk- og kommunikationssikkerhed

Ethernet er en hardware standard, der findes standarder til forskellig slags forbindelser,

- 802.11 Wifi
- 802.15 Bluetooth
- 802.3 Ehternet kablet

SSID - navnet på WiFi

CPE - Customer premise equiepment

Subnet har en ip range

Routing - pakker mellem subnets?

Firewall filtrere på

- ip
- mac
- tcp / udp 
- port nr

Alle har source og destinaiton fks afsender ip og modtager ip

Next gen firewall kan filtrere på indhold fra afsender også, foreksempel om der er malicious indhold

Man kan sætte flere firewalls op, lidt ligesom middleware hvor man i den første firewall eksempelvis kan filtrere ip adresser fra (90% af alle kald), den anden firewall kan så kontrollere kaldet for malicious data (siden 90% er filtreret fra, er der ikke så meget data at gå igennem)

Subnet (læs op)

net maske (læs op)

mac adresse (læs op)

VLANs (læs op) - tagged VLAN kalder man også for trunk. VLANs er associeret med et subnet.

CIS er en best practice til at kontrollere om man har implemeneret sikkerhed ordentligt

en router ruter mellem subnets

Virtulization

nat/natting er når en virtuel maskine bruger hostens netværk

til at ændre tastetur layout i kommandolinje - wsconsctl

**Forskellige måder at skrive subnets på**

255.255.255.0 - Decimal

11111111.11111111.11111111.00000000 - Binært

IpAdresse /24 - 24 udgør hvor mange bit der er sat af til nætværket, der er at 24 ud af 32 bit af til netværket, og man kan derfor se der er 8 bit af til enheder der kan være tilsluttet netværket

CPE - customer provide equipment

DHCP - automatisk uddeling af ip til enheder der hopper påå netværket, uden dette ville man manuelt skulle sætte ipen på enheden for at enheden kunne kommunikere på netværket.

Default gateway - hvor enheden sender pakkerne hen for at få dem til internettet.

CEL (serielle forbindelser?) forbindelse

IPAM (IP Adress manager)

## Intro til it sikkerhed


